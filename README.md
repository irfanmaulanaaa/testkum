# RobotFramework for Automation Testing

[http://robotframework.org/](http://robotframework.org/)


### Pre-Requisites
* pip
* robotframework
* seleniumlibrary


### Installation

- pip  (download https://bootstrap.pypa.io/get-pip.py)
    ```sh
    $ python get-pip.py
    ```    
- robotframework  
    ```sh
    $ pip install robotframework
    ```
- selenium2library  
    ```sh
    $ pip install robotframework-selenium2library==3.0.0b1
    ```


### How To Run
1. Clone this repo
2. Run all testcase
    ```
    $ robot /testcases
    ```  
    ###### this is sample command for running single testcase:
    ```sh
	  $ robot testcases/comment.robot
	  $ robot testcases/news.robot
	  $ robot testcases/registration_login.robot
    ```

### Report
    Report and log will generate after automation finished
    ```
    Open report.html
    Open log.html
    ```
