*** Settings ***
Library     Selenium2Library



*** Variables ***
#=======facebook==============================
${button_fb}        Login with Facebook
${window_fb}        Facebook
${email_fb}         id=email
${pass_fb}          id=pass
${login_fb}         id=u_0_0
${confim_fb}        name=__CONFIRM__
#==============================================

#=======google=================================
${button_google}    Login with Google +
${window_google}    Sign in - Google Accounts
${email_google}     name=identifier
${pass_google}      name=password
${next_google}      xpath=//span[contains(text(), 'Next')]
${allow_google}     xpath=//span[contains(text(), 'ALLOW')]
#==============================================

${popup_notif}      TIDAK
${image_profile}    xpath=//img[@class="img-30 img-circle"]
${window_home}      kumparan - Platform Media Kolaboratif Indonesia
${window_login}     Login
${select_topic}     /topic/news/terkini


*** Keywords ***
close popup notification
    Wait Until Page Contains    ${popup_notif}
    Click Button    ${popup_notif}

click login or sign up
    ${cek_popup}   Run Keyword And Return Status    Wait Until Page Contains    ${popup_notif}
    Run Keyword If    ${cek_popup}==True     close popup notification
    Sleep    5s
    Click Link    /login

click login with facebook
    Wait Until Page Contains    Login with Facebook
    Sleep    5s
    Click Button     ${button_fb}
    Wait Until Keyword Succeeds   20s   3s    Switch to Sign In facebook
    Title Should Be    ${window_fb}

Switch to Sign In facebook
    Select Window     ${window_fb}

input user facebook
    [Arguments]   ${username}   ${pass}
    Wait Until Element Is Visible    ${email_fb}
    Input Text    ${email_fb}    ${username}
    Input Text    ${pass_fb}     ${pass}
    Click Button    ${login_fb}

click login with facebook 2
    Wait Until Page Contains    Login with Facebook
    Sleep    5s
    Click Button     ${button_fb}

confirm user facebook
    Wait Until Element Is Visible    ${confim_fb}
    Click Button    ${confim_fb}

select topic
    ${cek_topic}   Run Keyword And Return Status    Wait Until Page Contains Element    ${select_topic}
    Run Keyword If    ${cek_topic}==True    Click Link    ${select_topic}

check success Login
    Sleep    3s
    ${cek_window}   Run Keyword And Return Status    Select Window   ${window_login}
    Sleep    3s
    Run Keyword If    ${cek_window}==False    Select Window   ${window_home}
    select topic
    Wait Until Element Is Visible    ${image_profile}

click login with google
    Wait Until Page Contains    Login with Google +
    Sleep    5s
    Click Button     ${button_google}
    Wait Until Keyword Succeeds   20s   3s    Switch to Sign In google
    Title Should Be    ${window_google}

click login with google 2
    Wait Until Page Contains    Login with Google +
    Sleep    5s
    Click Button     ${button_google}


Switch to Sign In google
    Select Window     ${window_google}

input user google
    [Arguments]   ${username}   ${pass}
    Wait Until Element Is Visible    ${email_google}
    Input Text    ${email_google}    ${username}
    Click Element    ${next_google}
    Wait Until Element Is Visible    ${pass_google}
    Input Text    ${pass_google}     ${pass}
    Sleep    3s
    Click Element    ${next_google}


confirm user google
    Wait Until Element Is Visible    ${allow_google}
    Sleep    3s
    Click Element    ${allow_google}

reset
      Delete Cookie    facebook.com
      Delete All Cookies
      Reload Page


logout
    Wait Until Keyword Succeeds    10s   3s        check success Login
    Click Element    ${image_profile}
    Wait Until Page Contains    Log Out
    Click Link    Log Out
