*** Settings ***
Library     Selenium2Library



*** Variables ***
${button_comment}    Komentar
${input_comment}     xpath=//textarea[@id="newCommentTextArea"]
${button_post}       Post


*** Keywords ***

click komentar
    Click Link    Komentar

input comment
    [Arguments]   ${comment}
    Wait Until Element Is Visible    ${input_comment}
    Input Text    ${input_comment}    ${comment}
    Set Global Variable    ${comment}
    Click Button     ${button_post}

should be redirect to page login
    Wait Until Keyword Succeeds    10s    5s    Title Should Be    Login

check result comment
    Sleep    5s
    Execute Javascript    window.scrollTo(1, 400);
    Wait Until Page Contains    ${comment}


Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute JavaScript    window.scrollTo(${x_location},${y_location})
