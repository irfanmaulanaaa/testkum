*** Settings ***
Library  Selenium2Library



*** Variables ***
${link_pict}         xpath=//div[@class="panel-body card-content"]/a[@class="btn-block"][1]
${link_title}        xpath=//div[@class="panel-body card-content"]/a[@class="btn-block"][2]
${news_title_cc}     xpath=//H4
${news_title_body}   xpath=//H1

*** Keywords ***
back to home
    Go To    https://kumparan.com/
    Wait Until Page Contains    kumparan

click news from picture
    ${title_cc}   Get Text    ${news_title_cc}
    Set Global Variable    ${title_cc}
    Click Link    ${link_pict}

click news from title
    ${title_cc}   Get Text    ${news_title_cc}
    Set Global Variable    ${title_cc}
    Click Link    ${link_title}

title content card must be same with title body
    Wait Until Element Contains    ${news_title_body}    ${title_cc}
    back to home
