*** Settings ***
Library     Selenium2Library
Resource    ../components/registration_login_keywords.robot


Documentation   testing for registration and login from facebook and google


*** Test Cases ***
registration with facebook
    [Tags]  positive test
    #gunakan acc facebook yg belum pernah terkoneksi dengan kumparan
    Open Browser    https://kumparan.com/   googlechrome
    click login or sign up
    click login with facebook
    input user facebook    testkumparan2@gmail.com    kumparan123
    confirm user facebook
    check success Login
    Close Browser

login with facebook (browser not already login facebook)
    [Tags]  positive test
    #gunakan acc facebook yg sudah pernah terkoneksi dengan kummparan
    Open Browser    https://kumparan.com/   googlechrome
    click login or sign up
    click login with facebook
    input user facebook    testkumparan@gmail.com    kumparan123
    check success Login
    logout

login with facebook (browser already login facebook)
    [Tags]  positive test
    Open Browser    https://kumparan.com/   googlechrome
    click login or sign up
    click login with facebook
    input user facebook    testkumparan@gmail.com    kumparan123
    check success Login
    logout
    click login or sign up
    click login with facebook 2
    check success Login
    Close Browser

registration google
    [Tags]  positive test
    #gunakan acc google yg belum pernah terkoneksi dengan kumparan
    Open Browser    https://kumparan.com/   googlechrome
    click login or sign up
    click login with google
    input user google     testkumparan2@gmail.com    kumparan123
    confirm user google
    check success Login
    Close Browser

login with google (browser not already login google)
    [Tags]  positive test
    #gunakan acc google yg sudah pernah terkoneksi dengan kummparan
    Open Browser    https://kumparan.com/   googlechrome
    click login or sign up
    click login with google
    input user google   testkumparan@gmail.com    kumparan123
    check success Login
    logout

login with google (browser already login google)
    [Tags]  positive test
    Open Browser    https://kumparan.com/   googlechrome
    click login or sign up
    click login with google
    input user google   testkumparan@gmail.com    kumparan123
    check success Login
    logout
    click login or sign up
    click login with google 2
    check success Login
    Close Browser
