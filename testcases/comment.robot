*** Settings ***
Library     Selenium2Library
Resource    ../components/registration_login_keywords.robot
Resource    ../components/news_keywords.robot
Resource    ../components/comment_keywords.robot


Documentation   testing for add comment


*** Test Cases ***
comment without login
    [Tags]  negative test
    Open Browser    https://kumparan.com/   googlechrome
    close popup notification
    click komentar
    input comment    test comment
    should be redirect to page login
    Close Browser

comment with login
    [Tags]  positive test
    Open Browser    https://kumparan.com/   googlechrome
    login with fb
    click komentar
    input comment    wow
    check result comment
    Close Browser




*** Keywords ***
login with fb
    click login or sign up
    click login with facebook
    input user facebook    testkumparan@gmail.com    kumparan123
    check success Login
