*** Settings ***
Library     Selenium2Library
Resource    ../components/news_keywords.robot


Suite Setup     Open Browser     https://kumparan.com/   googlechrome
Suite Teardown  Close Browser

Documentation   testing for user see news


*** Test Cases ***
see news from click picture content
    [Tags]  positive test
    click news from picture
    title content card must be same with title body

see news from click title content
    [Tags]  positive test
    click news from title
    title content card must be same with title body
